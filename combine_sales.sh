#!/usr/bin/env bash
cd processed
pattern="*Envysion_Sales*.csv"
files=( $pattern )
head -1 $files > AllSales.csv
tail -q -n+2 $pattern >> AllSales.csv


typical usage:
1. download raw b2b files from FTP site into the "datafiles" directory
2. run "pyton clean_files.py"
3. import the file into mysql or, if you downloaded several files,
use the "combine" bash scripts to make one big file csv file.

Use the clean_files.py script to format dates form direct mysql import and
remove spaces from the column names.  It will process anything in the
datafiles directory.

If you have a bunch of files you want to combine into one for
convenience of importing into mysql, you can use:
combine_sales.sh
combine_inventory.sh
To process all the files in the processed dir and create one big .csv file

If you need to make a new table CREATE statement from a datafile,
you can use csvsql from csvkit to create a table create statement.
example:

%>csvsql --dialect mysql AllSales.csv > sales_table.sql

You may want to expand any varchar cols to be a bit larger in case the
representative data used doesn't have max length data.

csvkit can be found here:
https://csvkit.readthedocs.io/en/1.0.2/
https://csvkit.readthedocs.io/en/1.0.2/tutorial/1_getting_started.html#installing-csvkit


I've used DataGrip and SequelPro to import the "All" csv files.
You can probably also use the Table Data Import wizard of Mysql Workbench.
I tried csvsql --import but had no luck building supporting python 3 libs needed.
Also no luck with MYSQL INFILE loading tools or mysqlimport

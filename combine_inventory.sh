#!/usr/bin/env bash
cd processed
pattern="*Envysion_InventoryOnHand*.csv"
files=( $pattern )
head -1 $files > AllInventoryOnHand.csv
tail -q -n+2 $pattern >> AllInventoryOnHand.csv

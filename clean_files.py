import csv
import os
from datetime import datetime
from dateutil.parser import parse

def fmt_date_mysql(val):
    try:
        return parse(val).strftime("%Y-%m-%d %H:%M:%S")
    except ValueError:
        return val

def process_file(fname):

    print("Processing... "+fname)
    outname = os.path.splitext(os.path.split(fname)[1])[0]+"_mysql_clean.csv"
    with open(os.path.join("processed", outname), 'w') as outcsvfile:
        spamwriter = csv.writer(outcsvfile, quoting=csv.QUOTE_MINIMAL)
        with open(fname, newline='') as csvfile:
            reader = csv.DictReader(csvfile)
            for idx, row in enumerate(reader):
                # clean up the header row, remove spaces in column
                # table names are nicer without spaces :)
                if idx == 0:
                    l = []
                    for k in reader.fieldnames:
                        l.append(k.replace(" ", ""))
                    spamwriter.writerow(l)
                else:
                    l = []
                    for k in reader.fieldnames:
                        # convert any fields with "date" in them
                        # to format mysql understands easily.
                        # also keep order of fields the same as
                        # the headers.
                        if 'date' in k.lower():
                            row[k] = fmt_date_mysql(row[k])
                        l.append(row[k])
                    spamwriter.writerow(l)
    print("Done " + fname)

# main
for file in os.listdir("datafiles"):
    if file.endswith(".csv"):
        process_file(os.path.join("datafiles", file))
